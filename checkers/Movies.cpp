#include<iostream>
#include "Movies.h"

Node::Node(int Val)
	: val(Val)
	, left(0)
	, right(0)
	, x(0), y(0)
	, fig_id(0)
{
}

Node::Node(int Val, int X, int Y, int Id)
	: val(Val)
	, left(0)
	, right(0)
	, x(X), y(Y)
	, fig_id(Id)
{
}

Movies::~Movies()
{
	DestroyNode(Root);
}

Movies::Movies() : Root(0)
{
}

void Movies::insert(int val, int x, int y, int id) {
	Node** cur = &Root;
	while (*cur) {
		Node& node = **cur;
		if (node.left == NULL) {
			cur = &node.left;
		}
		else if (node.right == NULL) {
			cur = &node.right;
		}
		else return;
		/*if (val < node.val) {
			cur = &node.left;
		}
		else if (val >= node.val) {
			cur = &node.right;
		}
		else {
			cur = &node.left;
			//return;
		}*/
	}
	*cur = new Node(val, x, y, id);
}

const void Movies::show() {
	Node* node = getRoot();
	if (node!=nullptr) {
		std::cout << "id: " << node->fig_id << ". x: " << node->x << ". y: " << node->y << std::endl;
		showTree(node->left);
		showTree(node->right);
	}
}

const void Movies::showTree(Node* node) {
	if (node!=NULL) {
		std::cout << "id: " << node->fig_id <<". x: "<< node->x << ". y: "<<node->y<< std::endl;
		showTree(node->left);
		showTree(node->right);
	}
}

void Movies::DestroyNode(Node* node) {
	if (node != nullptr) {
		DestroyNode(node->left);
		DestroyNode(node->right);
		delete node;
	}
}
