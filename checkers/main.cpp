#include <iostream>
//#include <SFML/Graphics.hpp>
#include <vector>
#include <map>
#include "figure.cpp"

using namespace sf; //��������� ��������� SFML

//Sprite f[24]; //������� � �������� 
Figure* f[24];
short size = 56; //������ ������ ����� � ������� �������
short padding = 28; //���������� ������� �� ����� �����, �� ������ 

std::multimap <int, std::pair<int, int>> moveList;

void showFild(int arr[8][8]);
void placing(); //�������� ������� ����������� ����� �� �����
void move();

short board[8][8] = {
	{ 1,0,1,0,1,0,1,0 },
	{ 0,1,0,1,0,1,0,1 },
	{ 1,0,1,0,1,0,1,0 },
	{ 0,0,0,0,0,0,0,0 },
	{ 0,0,0,0,0,0,0,0 },
	{ 0,2,0,2,0,2,0,2 },
	{ 2,0,2,0,2,0,2,0 },
	{ 0,2,0,2,0,2,0,2 },
};


int main(void) {
	setlocale(LC_ALL, "Russian");


	RenderWindow window(VideoMode(504, 504), "Checkers", Style::Close);
	window.setFramerateLimit(60);

	Texture boardTexture;
	boardTexture.loadFromFile("images/board.png"); //��������� �����
	Sprite boardSprite;
	boardSprite.setTexture(boardTexture);

	/*
	Texture blk; //������ ��������, ����� �������� �� �� �����
	Texture wht; //� ������ �� ��� ������� �����
	for (short i = 0; i < 12; i++) {
		wht.loadFromFile("images/white.png", IntRect(0, 0, size, size)); //��������� ��������� �����, ����� ������� ������� 
																		 //����� �������� ������ �������, � ������ ������ �� ����� 0 0 �� ����� �����
		f[i].setTexture(wht); //��������� ������ �������� ����� �� ��������
	}
	for (short i = 12; i < 24; i++) {
		blk.loadFromFile("images/black.png", IntRect(0, 0, size, size)); //��������� ��������� �����, ����� ������� ������� 
		f[i].setTexture(blk); //��������� ������ �������� ����� �� ��������
	}
	*/

	Texture black;
	black.loadFromFile("images/black.png", IntRect(0, 0, size, size));
	Texture white;
	white.loadFromFile("images/white.png", IntRect(0, 0, size, size));
	for (short i = 0; i < 12; i++) {
		f[i] = new Figure('b', i, Sprite(black), (*board));
	}
	for (short i = 12; i < 24; i++) {
		f[i] = new Figure('w', i, Sprite(white), (*board));
	}
	

	placing(); //������� ����������� �����

	bool isMove = false; //��� ������ ���� �� ������ 
	float dX = 0.0; //���������� ������� �� �
	float dY = 0.0; //���������� ������� �� �
	short oldX = 0;
	short oldY = 0;

	short n = 0;

	while (window.isOpen()) {
		Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) window.close();

			Vector2i pos = Mouse::getPosition(window);//�������� ����� �������
			for (short i = 0; i < 24; i++) {
				if (event.type == Event::MouseButtonPressed)//���� ������ ������� ����
					if (event.key.code == Mouse::Left)//� ������ �����
						if (f[i]->sprite.getGlobalBounds().contains(pos.x, pos.y))//� ��� ���� ���������� ������� �������� � ������
						{
							n = i;
							oldX = f[i]->sprite.getPosition().x;
							oldY = f[i]->sprite.getPosition().y;
							//std::cout << "oldX=" << oldX << ". oldY=" << oldY << "\n";
							std::cout << "isClicked! " << i << "\n";//������� � ������� ��������� �� ����
							dX = pos.x - f[i]->sprite.getPosition().x;//������ �������� ����� �������� ������� � �������.��� ������������� �������
							dY = pos.y - f[i]->sprite.getPosition().y;//���� ����� �� ������
							isMove = true;//����� ������� ������							
						}
						
			}

			if (event.type == Event::MouseButtonReleased && isMove)//���� ��������� ������� � ������� ��������
				if (event.key.code == Mouse::Left) { //� ������ �����
					//isMove = false; //�� �� ����� ������� ������
					//f[n]->sprite.setColor(Color::White);//� ���� ��� ������� ����

					if (pos.x > 28 && pos.x < 476 && pos.y > 28 && pos.y < 476) { //����� ����� ���� ���������� ������ �� �������, �� ������ �� �������
						short x = abs(8 - (472 - pos.x) / 56); //���� ����� ������ �� ����
						short y = abs(8 - (472 - pos.y) / 56);
						short ox = abs(8 - (472 - oldX) / 56);
						short oy = abs(8 - (472 - oldY) / 56);
						std::cout << "oldX=" << ox << ". oldY=" << oy << "\n";
						std::cout << "X=" << x << ". Y=" << y << "\n";
						if (x != ox || y != oy) {
							//std::cout << "board[x][y]=" << board[x][y] << ". board[ox][oy]=" << board[ox][oy] << "\n";
							board[y - 1][x - 1] = board[oy - 1][ox - 1];
							std::cout << "board[y][x]=" << board[y - 1][x - 1] << ". board[oy][ox]=" << board[oy - 1][ox - 1] << "\n";
							board[oy - 1][ox - 1] = 0;
							//board[y - 1][x - 1] = board[oy - 1][ox - 1];

							//x = x * 56 - 28;   //����� ��� � �����, ������� �������� ��� �� 8, ���� ������ 
							//y = y * 56 - 28;	//�������� �� ������ ������ � �������� ������

							f[n]->sprite.setPosition(x * 56 - 28, y * 56 - 28);		//������ �������
						}
						//isFirst = false;
						//f[n].setPosition(-50, -50);
						//f[n].setPosition(pos.x - dX, pos.y - dY); //� ��� ������ ��������������. ��� ������
					}
					else f[n]->sprite.setPosition(oldX, oldY);	//���� ������� �� ������� �����, �� ���������� ������� 
					isMove = false;
				}
			//}

			if (isMove) {//���� ����� �������
				//f[n].setColor(Color::Red);			 //������ ������ � �������, ����� ���� �������, ��� �� ������ � ����� �������
				f[n]->sprite.setPosition(pos.x - dX, pos.y - dY);//����������� ���� ����� �������, ����� ��������� ������ � ��������, ����� ����� 
			}



			if (event.type == Event::KeyPressed)
				if (event.key.code == Keyboard::BackSpace) {
					f[0]->boardPrint();
					//for (short i = 0; i < 8; i++) {
					//	for (short j = 0; j < 8; j++) {
					//		std::cout << board[i][j] << " ";
					//	}
					//	std::cout << std::endl;
					//}
				}

			if (event.type == Event::KeyPressed)
				if (event.key.code == Keyboard::Space) {
					/*move();
					for (auto it = moveList.begin(); it != moveList.end(); it++) {
						//std::cout << "fig: " << it->first << " y:" << it->second.first << " x:" << it->second.second << std::endl;
						std::cout << "fig: " << it->first << " y:" << it->second.first << " x:" << it->second.second << std::endl;
					}*/
					f[0]->move.DestroyNode(f[0]->move.Root);
					for (int i = 0; i < 24; i++) {
						f[i]->movies();
					}
					//f[12]->show();
				}
		}
		// ��������� ����������� �������� �� ���������
		window.clear(); //������� ����
		window.draw(boardSprite); //������ �����
		for (short i = 0; i < 24; i++) window.draw(f[i]->sprite); //������ ������ 
		window.display();
	}

	//system("Pause");
	return 0;
}


void placing() {
	short k = 0;
	for (short i = 0; i < 8; i++) {
		for (short j = 0; j < 8; j++) {
			short n = board[i][j];
			if (!n) continue;
			//std::cout << "Old position = x: " << f[k]->sprite.getPosition().x << " y: " << f[k]->sprite.getPosition().y << std::endl;
			f[k]->sprite.setPosition(size*j + 28, size*i + 28);
			//std::cout << "New position = x: " << f[k]->sprite.getPosition().x << " y: " << f[k]->sprite.getPosition().y << std::endl << std::endl << std::endl;;
			board[i][j] = k + 1;
			k++;
		}
	}
}

//void showFild(int** arr){
void showFild(int arr[8][8]) {
	for (int i = 0; i<8; i++) {
		for (int j = 0; j<8; j++) {
			std::cout << arr[i][j] << " ";
		}
		std::cout << std::endl;
	}
}

void move() {	//��������� ������ �����

	//short k = 0;

	moveList.clear(); //� ������ ������� ������� ������ ������ ����� � ��������� ������ 

	for (int i = 0; i < 8; i++) {
		for (short j = 0; j < 8; j++) {
			short n = board[i][j];	   //�������� ���������� � ������ ����� 
			if (!n) continue;		   //���� ��� ����� ����, �� ������ ���, ��� ������ 
			//std::cout <<  "   " << k << std::endl;
			if (n <= 12) {
				if (i + 1 < 8) {
					if (j + 1 < 8 && board[i+1][j+1] == 0)
						moveList.emplace(n, std::make_pair(i + 1, j + 1));	//���� ����� ����� ����� ����� 
					if(j - 1 > 0 && board[i+1][j-1] == 0)
						moveList.emplace(n, std::make_pair(i + 1, j - 1));
				}
			}
			if (n > 12) {

				moveList.emplace(n, std::make_pair(i + 1, j));	//���� ����� ����� ������ ����� 
			}
		}
		//k++;
	}
}