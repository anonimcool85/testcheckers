#pragma once

class Node {
public:
	Node(int Val);
	Node(int Val, int X, int Y, int Id);
	int val;
	int x, y, fig_id;
	Node* left;
	Node* right;
};

class Movies
{
public:
	Movies();
	~Movies();
	void insert(int mot, int x, int y, int id);
	const void showTree(Node* node);
	const void show();
	//Node getRoot() { return *Root; }
	Node* getRoot() { return Root; }
	Node *Root;
	static void DestroyNode(Node* node);
private:
	//Node* Root;
	//static void DestroyNode(Node* node);
};

